#define N_SENSORS 4
#define TIMEOUT 10000
#define TRIGGER 500

#define TRG_A 2
#define ECH_A 3
#define TRG_B 4
#define ECH_B 5
#define TRG_C 6
#define ECH_C 7
#define TRG_D 8
#define ECH_D 9

#define BTN_E 10

int tiggers[4] = {TRG_A,TRG_B,TRG_C,TRG_D};
int echoes[4] = {ECH_A,ECH_B,ECH_C,ECH_D};
char chr_on[4] = {'A','B','C','D'};
char chr_off[4] = {'a','b','c','d'};
char chr_act[4] = {0,0,0,0};

void setup() {
  // put your setup code here, to run once:
  for(int i=0;i<N_SENSORS;i++) {
    pinMode(tiggers[i], OUTPUT);
    digitalWrite(tiggers[i], LOW);
    pinMode(echoes[i], INPUT);  
  }
  pinMode(BTN_E, INPUT_PULLUP);  
  Serial.begin(9600);
  Serial1.begin(9600);
}

int sensor = 0;
int state = 0;
int lastVal[4] = {0,0,0,0};
long t_btn[4] = {0,0,0,0};
long d_btn[4] = {10000,10000,10000,10000};
int last_btn_val = 1;

void loop() {
  long val;
  long new_dst;
  int new_btn_val = digitalRead(BTN_E);

  if(new_btn_val<last_btn_val) {
    Serial.print("E");
    Serial1.print("E");
  }
  last_btn_val = new_btn_val;
  
  switch(state) {
    case 0:
      // Restart sensor counter if needed
      if(sensor>N_SENSORS) sensor = 0;
      
      // Send trigger and wait for echo
      digitalWrite(tiggers[sensor], HIGH);
      delayMicroseconds(15);
      digitalWrite(tiggers[sensor], LOW);
      
      t_btn[sensor] = micros();
      
      state = 1;
      break;
    case 1:
      val = digitalRead(echoes[sensor]);
      if(lastVal[sensor]>val | ((micros()-t_btn[sensor])> TIMEOUT)) {
        // Flanco negativo
        new_dst = micros()-t_btn[sensor];

        if((new_dst < TRIGGER) && chr_act[sensor]!=chr_on[sensor]) {
          chr_act[sensor] = chr_on[sensor];
          Serial.print(chr_on[sensor]);
          Serial1.print(chr_on[sensor]);
        }

        if((new_dst > TRIGGER) && chr_act[sensor]!=chr_off[sensor]) {
          chr_act[sensor] = chr_off[sensor];
          Serial.print(chr_off[sensor]);
          Serial1.print(chr_off[sensor]);
        }
        
        d_btn[sensor] = new_dst;

        state = 0;
        lastVal[sensor] = val;
        sensor++;
        delay(10);
      } else if(lastVal[sensor]<val) {
        // Flanco positivo
        t_btn[sensor] = micros();
        lastVal[sensor] = val;
      }
      
      break;
  }
}
